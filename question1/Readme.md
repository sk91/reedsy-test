## Question 1

Tell us about one of your commercial projects with Node.js and/or AngularJS.

## Answer

#### Luminati Proxy Manager

https://github.com/luminati-io/luminati-proxy

##### What is Luminati?

A proxy service that provides residential and datacenter ips. The recedential ips are actual machines around the world. Could be cell-phone, laptops, pc's and more.

##### What does the proxy manager do?

The proxy manager does a few things:

*   Hides api complexity by providing a ui to preconfiure proxies and exposes an more simple api

*   Provides users with different scrapping options: like random sessions, ip pools, rules and more

*   Shows usage stats

And more...

##### Structure:

This product is used by end users. Installed on their machines locally or on they'r servers.

*   Rest Api + Web Server using Express.js
*   A number (configured by users) of proxy servers supporting HTTP/S, Socks5 protocols
*   Frontend angular 1.6 in process of migration to react.

##### Usage

The user would run the process in one of the 3 modes:

*   simple command line executable
*   prebuilt exe (via electron)
*   deamon mode (using pm2)

Login to the UI and configure several ports to run proxies. Each port would have a different configuration the user needs.

Than the user would configure a client to use with the ports he oppened (firefox/curl/some customer scrapper)

##### Challanges

The product was created in an MVP ideology. But unfortunately never got to be rewritten properly to get out of teh MVP phase.

So when I came to the project I faced the following challanges:

*   absolutely unmanageble code - many people gone has trough and wrote hacks upon hacks
*   state was very distributed and modified by in many places
*   memory leaks
*   unexplainable high cpu in some cases
*   file descriptor leaks
*   security concerns
*   state prevented distributing the process
*   poor logging
*   no feedback from users

All of this needed to be fixed in a gradual way. Constantly releasing new features.

##### Solutions

> *   absolutely unmanageble code - many people gone has trough and wrote hacks upon hacks
> *   state was very distributed and modified by in many places

A lot of time was spent on refactoring, separating concerns, breaking into smaller peaces and simplifiying the code. Also a lot of work was done in proper error handling and reporting.

> *   memory leaks

Most was gone in refactoring, but for some, spent alot of time taking memory snapshots and traking them down. All solved in the end.

> *   file descriptor leaks

Solved in 4 ways

*   Automatic limit raise if user has permissions to do so
*   Implemented a service that will track open fd level and will force the proxies to refuse new request whe levels where critical
*   Fixing bugs for sockets that where not closed (async flows/errors where not handled corrctly.)
*   Optimized architecture:
    Initially, each port to support multiple protocols would actually listen on 3 ports with different servers (http, https and socks5). And will open sockes between them to pipe requests from one to another.
    This was replaced by a single Net server that would read the first bite of the arriving connection and from that will derive which protocol has been used. Then it would use different parsers to handle the connection.

> *   security concerns

Implemented features for security (like whitelist ips, process role and more)

> *   poor logging

Set up logging in different levels allowing to effectively debug the process, and added mechanisems to filter thous logs.

> *   no feedback from users

*   Added the ability to send events and error to our logging servers.

Errors would contain stack traces and recent log heads

*   Added a mechanisem to detect suddent crash (via constantly writing status to file)

*   Added bug report features that would send info about system, log head and bug description
