## Question 1

Detail how would you persist in data/present a schema to store several versioned text-based documents. It should allow to:

*   save a version representing a document state
*   keep the versions list/document history for browsing
*   browse a previous version and
*   visualize the changes/diff between two versions.

Strive for storage size efficiency.

## Answer

For each document I would store a series of diff like that:

```
{
id: "DOC_ID",
revision: 1,
changes: diff1
},
{
id: "DOC_ID",
revision: 2,
changes: diff2
}
...
```

#### save operation:

*   fetch last version
*   diff with changed version
*   create new revision object and save a diff to it

#### fetch operation:

given a revision

*   get all revision records sorted by revision and <= given revision
*   iterate on the revisions from oldest to newest applying patch of the diff on
    the previous revision

#### visualize diff

given rev1 and rev2

*   fetch rev1
*   fetch rev2
*   diff rev1, rev2

return diff to ui. aplly ui logic (probably some standard lib) to display the diff;

(here we can use a different diff for convinience that might be less optimized but supported by standatd tools)

#### diff operation

it can be implemented in many ways
basic implementation is using standatd unix diff command.
But it can be very optimized via other algoritms.

#### patch operation

depending on how diff was implemented simply applies diff changes on a revision
