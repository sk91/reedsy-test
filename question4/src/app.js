'use strict';
import angular from 'angular';
import DateChooser from './date-chooser/date-chooser.js';
import appTpl from './app.html';

const mod = angular.module('ReedsyTest', ['ReedsyTest.DateChooser']);

mod.directive('reedsyApp', [()=>({
	template: appTpl
})]);

export default mod;

