import {DateChooserCtl, MonthsService} from './date-chooser.js';

describe('date-chooser', ()=>{
	describe('DateChooserCtl', ()=>{
		describe('init', ()=>{
			let baseTime, ctl;
			beforeEach(()=>{
				baseTime = new Date();
				jasmine.clock().install();
				jasmine.clock().mockDate(baseTime);
				ctl = new DateChooserCtl();
				spyOn(ctl, 'updateDate');
				ctl.$onInit();
			});
			afterEach(()=>{
				jasmine.clock().uninstall();
			});
			it('should set update handler', ()=>{
				expect(ctl.handleDateChange).toBeDefined();
			});
			it('should update date', ()=>{
				expect(ctl.updateDate).toHaveBeenCalled();
			});
		});
		describe('updateDate', ()=>{
			let ctl;
			beforeEach(()=>{
				ctl = new DateChooserCtl(new MonthsService());
			});
			it('should set all vars', ()=>{
				let date = new Date(2018, 1, 3);
				ctl.updateDate(date);
				expect(ctl.ts).toEqual(date.getTime());
				expect(ctl.year).toEqual(2018);
				expect(ctl.day).toEqual(3);
				expect(ctl.month).toEqual('Feb');
			})
		});
	})
	describe('MonthsService', ()=>{
		let months;
		beforeEach(()=>{
			months = new MonthsService();
		});
		describe('abbrToMonth', ()=>{
			const t = (abbr, month)=>it(`${abbr}->${month}`, ()=>{
				expect(months.abbrToMonth(abbr)).toEqual(month);
			});
			t('Jan', 0);
			t('Jun', 5);
			t('Sep', 8);
			t('FAKE', 0);
		});
		describe('monthToAbbr', ()=>{
			const t = (month, abbr)=>it(`${month}->${abbr}`, ()=>{
				expect(months.monthToAbbr(month)).toEqual(abbr);
			});
			t(0, 'Jan');
			t(5, 'Jun');
			t(8, 'Sep');
			t(13124124, 'Jan');
			t(-1, 'Jan');
		});
		describe('daysInMonth', ()=>{
			const t = (year, month, days)=>it(`${year} ${month}->${days}`, ()=>{
				expect(months.daysInMonth(year, month)).toEqual(days);
			});
			t(2018, 0, 31);
			t(2018, 1, 28);
			t(2018, 5, 30);
		})
	})
});