import angular from "angular";
import dateChooserTpl from "./date-chooser.html";

const mod = angular.module("ReedsyTest.DateChooser", []);

export class MonthsService {
	constructor(){
		this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec']
	}
	getAll(){
		return this.months;
	}
	abbrToMonth(abbr){
		let month = this.months.indexOf(abbr);
		return month < 0 ? 0 : month;
	}
	monthToAbbr(month){
		if (month < 0 || month > this.months.length)
			month = 0;
		return this.months[month];
	}
	daysInMonth(year, month){
		return new Date(year, month+1, 0, 0, 0).getDate();
	}
}

export class DateChooserCtl {
	constructor(months){
		this.months = months;
	}
	$onInit(){
		this.handleDateChange = ()=>{
			const month = this.months.abbrToMonth(this.month);
			const updateDate = new Date(this.year, month, this.day);
			this.updateDate(updateDate);
		};
		this.updateDate();
	}
	updateDate(date){
		date = date || new Date();
		this.ts = date.getTime();
		this.day = date.getDate();
		this.month = this.months.monthToAbbr(date.getMonth());
		this.year = date.getFullYear();
	}
	getMonthOptions(){
		return this.months.getAll();
	}
	getDayOptions(){
		const daysInMonth = this.months.daysInMonth(this.year, this.months.abbrToMonth(this.month));
		return Array.apply(null, new Array(daysInMonth)).map((v, i)=>i+1);
	}
	getString(){
		return (new Date(this.ts)).toDateString();
	}
}

mod.service('monthsService', MonthsService);

mod.controller('ReedsyDateChooser', ['monthsService', DateChooserCtl]);

mod.directive('reedsyDateChooser', ()=>({
	template: dateChooserTpl,
	controller: 'ReedsyDateChooser',
	controllerAs: 'ctl',
	bindToController: true
}));

export default mod;