const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const mode = process.env.NODE_ENV||'development';
module.exports = {
	entry: {
		app: './src/app.js'
	},
	mode,
	output: {
		path: path.join(__dirname, 'dist'),
		filename: '[name].js'
	},
	devtool: 'cheap-module-source-map',
	devServer: {
		inline: true,
		stats: 'minimal',
		historyApiFallback: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loaders: [
					'ng-annotate-loader',
					'babel-loader'
				]
			},
			{
				test: /\.html$/,
				exclude: /node_modules/,
				loader: 'raw-loader'
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(['dist'], {
			root: __dirname,
			verbose: true,
			dry: false
		}),
		new HtmlWebpackPlugin({
			title: 'Reedsy Task',
			template: `./src/index.html`,
			inject: true
		})
	]
};