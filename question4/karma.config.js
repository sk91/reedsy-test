const webpackConfig = require('./webpack-test.config.js');

module.exports = config =>
	config.set({
		basePagh: '.',
		frameworks: ['jasmine'],
		reporters: ['mocha'],
		port: 9000,
		colors: true,
		logLevel: config.LOG_DEBUG,
		autoWatch: false,
		browsers: ['PhantomJS'],
		singleRun: true,
		autoWatchBatchDelay: 300,
		files: ['./src/**/*.spec.js'],
		preprocessors: {
			'./src/**/*.spec.js': ['webpack', 'sourcemap'],
		},
		webpack: webpackConfig,
		webpackMiddleware: {
			stats: 'minimal',
		},
	});
