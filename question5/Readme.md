## Task:

Implement a ‘model’ for **text edit operation** that encapsulates it's logic in Angular 1.x. An operation can be described as an array of three types of edits: { **move**: } to advance the caret, { **insert**: } to insert the string at caret, and { **delete**: } to delete a number of chars from the caret onwards. Implement the following methods:

- Operation.prototype.compose(operation) - Updates the operation by ‘adding’/composing it with another one
- Operation.compose(op1, op2) - Static method that returns a new operation composed by the two without changing any of them
- Operation.prototype.apply(string) - Applies the described operation on a string

Examples of compose:
```
[{ move: 1 }, { insert: 'foo' }] + [{ move: 6 }, { insert: 'bar' }] = [{ move: 1 }, { insert: 'foo' }, { move: 5}, { insert: 'bar' } ]

[{ move: 1 }, { insert: 'foo' }] + [{ move: 6 }, { delete: 2 }] = [{ move: 1 }, { insert: 'foo' }, { move: 5}, { delete: 2 } ]

```

Add test coverage as you see fit.