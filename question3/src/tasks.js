const uuidv4 = require('uuid/v4');
const EventEmmiter = require('events');
const child_process = require('child_process');
const debug = require('debug')('reedsy:tasks');
const STATUS_PENDING = 'pending';
const STATUS_PROCESSING = 'processing';
const STATUS_COMPLETE = 'complete';
const STATUS_ERROR = 'error';

class PriorityQueue {
	constructor(priority_cmp) {
		this.priority_cmp = priority_cmp;
		this.queue = [];
	}
	insert(itm) {
		debug('queue: inserting itm %s', itm.id);
		this.queue.push(itm);
		this.queue.sort(this.priority_cmp);
	}
	delete(itm) {
		let idx = this.queue.indexOf(itm);
		if (idx < 0) return itm;
		this.queue.slice(idx, 1);
		return itm;
	}
	next() {
		return this.queue.pop();
	}
	hasNext() {
		return this.queue.length > 0;
	}
}

const TYPE_PROCESSING_TIME = {
	html: 1000,
	pdf: 5000,
	default: 3000,
};

const PROGRESS_INTERVAL = 1000;

class TaskManager extends EventEmmiter {
	constructor() {
		super();
		this.tasks = {};
		this.running = false;
		this.queue = new PriorityQueue(TaskManager.priority_cmp);
		this.processors = {};
		this.idleProcessors = [];
	}
	static priority_cmp(t1, t2) {
		const prio1 = t1.create_ts + t1.proc_time;
		const prio2 = t2.create_ts + t2.proc_time;
		return prio2 - prio1;
	}
	notifyIdleProcessor(processor) {
		debug('mgr: processor idle %s', processor.id);
		this.idleProcessors.push(processor);
		setImmediate(this.processQueue.bind(this));
	}
	processQueue() {
		let task, processor;
		debug(
			'mgr: processing queue %s',
			this.running &&
				this.queue.hasNext() &&
				this.idleProcessors.length > 0,
		);
		while (
			this.running &&
			this.queue.hasNext() &&
			this.idleProcessors.length > 0
		) {
			processor = this.idleProcessors.pop();
			task = this.queue.next();
			debug(
				'mgr: assigning task %s to processor %s',
				task.id,
				processor.id,
			);
			processor.assignTask(task);
		}
	}
	run(processes = 1) {
		this.running = true;
		debug('mgr: starting with %s processes', processes);
		for (let i = 0; i < processes; i++)
			this.initProcessor(new TaskProcessor(this));
	}
	stop() {
		this.running = false;
		debug('mgr: stopping');
		for (let proc in this.processors) this.processors[proc].stop();
	}
	initProcessor(processor) {
		if (!processor.id) return;
		debug('mgr: initializing processor %s', processor.id);
		this.processors[processor.id] = processor;
		processor.run();
	}
	removeProcessor(processor) {
		processor.stop();
		delete this.processors[processor.id];
	}
	createTask(type, name, data) {
		const id = uuidv4();
		let task = {
			id,
			type,
			name,
			data,
			status: STATUS_PENDING,
		};
		task.create_ts = Date.now();
		task.proc_time =
			TYPE_PROCESSING_TIME[type] || TYPE_PROCESSING_TIME.default;
		debug('mgr: create task %O', task);
		this.tasks[id] = task;
		this.queue.insert(task);
		setImmediate(this.processQueue.bind(this));
		return task;
	}
	deleteTask(id) {
		let tsk = this.tasks[id];
		delete this.tasks[id];
		this.queue.delete(tsk);
		return tsk;
	}
	getTask(id) {
		return this.tasks[id];
	}
	updateTask(task) {
		if (!this.tasks[task.id]) return task;
		return Object.assign(this.tasks[task.id], task);
	}
}

class TaskProcessor {
	constructor(mgr) {
		this.mgr = mgr;
		this.worker = null;
		this.processing_task = null;
		this.running = false;
		this.id = uuidv4();
		this.msgHandler = msg => this.handleMessage(msg);
	}
	run() {
		debug('processor: starting %s', this.id);
		this.running = true;
		this.initWorker();
	}
	stop() {
		if (!this.running) return;
		this.running = false;
		if (!this.worker) return;
		this.worker.send({ cmd: 'processor:kill' });
	}
	initWorker() {
		debug('processor: starting worker for processor %s', this.id);
		console.log(__filename);
		this.worker = child_process.fork(__filename);
		this.worker.on('message', this.msgHandler);
		this.worker.on('disconnect', () => this.handleDisconnect());
	}
	uninitWorker() {
		this.worker.removeAllListeners('message');
		this.worker.removeAllListeners('disconnect');
		this.worker = null;
	}
	assignTask(task) {
		this.processing_task = task;
		this.worker.send({ cmd: 'task:assign', data: task });
	}
	handleMessage(msg) {
		const cmd = msg.cmd;
		debug('processor: recv message %s', cmd);
		switch (cmd) {
			case 'processor:ready':
				return this.handleReady(msg.data);
			case 'task:start':
				return this.handleStart(msg.data);
			case 'task:error':
				return this.handleError(msg.data);
			case 'task:complete':
				return this.handleComplete(msg.data);
			case 'task:progress':
				return this.handleProgress(msg.data);
		}
	}
	updateTask(task) {
		this.processing_task = this.mgr.updateTask(task);
	}
	handleProgress(task) {
		debug('processor: task %s progress %s', task.id, task.progress);
		this.updateTask({
			id: task.id,
			progress: task.progress,
		});
	}
	handleStart(task) {
		debug('processor: task start %s', task.id);
		this.updateTask({
			id: task.id,
			start_ts: task.start_ts,
			status: STATUS_PROCESSING,
		});
	}
	handleError(task) {
		debug('processor: task error %s', task.id);
		this.updateTask({
			id: task.id,
			status: STATUS_ERROR,
			error: task.error,
		});
		this.mgr.notifyIdleProcessor(this);
	}
	handleComplete(task) {
		debug('processor: task complete %s', task.id);
		this.updateTask({
			id: task.id,
			complete_ts: task.complete_ts,
			status: STATUS_COMPLETE,
		});
		this.mgr.notifyIdleProcessor(this);
	}
	handleDisconnect() {
		debug('processor: disconnected');
		if (
			this.processing_task &&
			this.processing_task.status != STATUS_COMPLETE
		) {
			this.updateTask({
				id: this.processing_task.id,
				status: STATUS_ERROR,
				error: 'Error processing task',
			});
		}
		this.uninitWorker();
	}
	handleReady() {
		this.mgr.notifyIdleProcessor(this);
	}
}

class TaskProcessorWorker {
	run() {
		debug('wkr: starting worker');
		process.on('message', this.handleMessage.bind(this));
		process.send({ cmd: 'processor:ready' });
	}
	stop() {
		process.removeAllListeners('message');
	}
	processTask(task) {
		this.taskStart(task);

		this.taskTimeout = setTimeout(() => {
			console.log(
				'task',
				task.id,
				'complete',
				'name:',
				task.name,
				'type:',
				task.type,
			);
			this.taskComplete(task);
		}, task.proc_time);

		this.progressInterval = setInterval(() => {
			let progress = (Date.now() - task.start_ts) / task.proc_time;
			this.taskProgress(task, Math.min(progress, 1));
		}, PROGRESS_INTERVAL);
	}
	taskStart(task) {
		debug('wkr: starting task %s', task.id);
		task.start_ts = Date.now();
		process.send({ cmd: 'task:start', data: task });
	}
	taskStop(task) {
		debug('wkr: stopping task %s', task.id);
		if (this.taskTimeout) clearTimeout(this.taskTimeout);
		if (this.progressInterval) clearInterval(this.progressInterval);
	}
	taskProgress(task, progress) {
		debug('wkr: task %s progress %s', task.id, progress);
		if (progress == 1) clearInterval(this.progressInterval);
		task.progress = progress;
		process.send({ cmd: 'task:progress', data: task });
	}
	taskError(task, error) {
		debug('wkr: task %s has an error: %s', task.id, error);
		this.taskStop(task);
		task.error = error;
		process.send({ cmd: 'task:error', data: task });
	}
	taskComplete(task) {
		debug('wkr: task %s is complete', task.id);
		task.complete_ts = Date.now();
		this.taskStop(task);
		process.send({ cmd: 'task:complete', data: task });
	}
	handleMessage(msg) {
		const { cmd } = msg;
		debug('wkr: got message %s', cmd);
		switch (cmd) {
			case 'processor:kill':
				return this.handleKill();
			case 'task:assign':
				return this.handleAssign(msg.data);
		}
	}
	handleAssign(task) {
		debug('wkr: received a task %O', task);
		this.processTask(task);
	}
	handleKill() {
		debug('wkr: received kill command, exiting');
		this.stop();
		process.exit();
	}
}

const main = () => {
	let worker = new TaskProcessorWorker();
	worker.run();
};

module.exports = {
	STATUS_PENDING,
	STATUS_COMPLETE,
	STATUS_PROCESSING,
	STATUS_ERROR,
	TYPE_PROCESSING_TIME,
	TaskManager,
	TaskProcessor,
	TaskProcessorWorker,
	PriorityQueue,
};

if (!module.parent) main();
