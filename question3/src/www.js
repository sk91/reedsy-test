const express = require('express');
const bodyParser = require('body-parser');
const debug = require('debug')('reedsy:www');
const { TaskManager } = require('./tasks.js');
const PORT = process.env.PORT || 3000;

const create_http_error = (message, status, code) => {
	let err = message;
	if (typeof err == 'string') {
		err = new Error(err);
	}
	err.status = status || 500;
	err.code = code;
	return err;
};

const init_request = app => {
	return (req, res, next) => {
		req.tasks_mgr = app.get('tasks_mgr');
		next();
	};
};

const error_handler = (err, req, res, next) => {
	const status = err.status || 500;
	const message = err.message || err;
	const code = err.code || 'unknown_error';
	if (err.code) res.statusMessage = code;
	res.status(status).json({ status, message, code });
};

const task_create = (req, res, next) => {
	if (!req.body) {
		return next(create_http_error('no data received', 400, 'no_data'));
	}
	let { type, name, data } = req.body;
	try {
		debug('creating task type:%s name:%s data:%O', type, name, data);
		let tsk = req.tasks_mgr.createTask(type, name, data);
		return res.json(tsk);
	} catch (err) {
		debug('error creating task %O', err);
		return next(create_http_error('server error', 500));
	}
};

const task_status = (req, res, next) => {
	const id = req.params.id;
	let task = req.tasks_mgr.getTask(id);
	if (!task) {
		return next(create_http_error('task not found', 404, 'not_found'));
	}
	return res.json(task);
};

const init = opt => {
	const app = express();
	app.use(init_request(app));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.post('/tasks', task_create);
	app.get('/tasks/:id', task_status);
	app.use(error_handler);
	app.set('tasks_mgr', new TaskManager());
	return app;
};

const run = (app, opt) => {
	app.listen(opt.port, () => debug('listening on port %s', opt.port));
	app.get('tasks_mgr').run();
};

const main = () => {
	const opt = { port: PORT };
	const app = init(opt);
	run(app, opt);
};

module.exports = { init, run };

if (!module.parent) main();
